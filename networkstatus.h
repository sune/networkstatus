/*
    Copyright (c) 2010-2011 Sune Vuorela <sune@vuorela.dk>

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without
    restriction, including without limitation the rights to use,
    copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following
    conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE.

*/
#ifndef NETWORKSTATUS_H
#define NETWORKSTATUS_H

#include <Plasma/Applet>
#include <Plasma/Svg>
#include <Solid/Networking>
#include <QTimer>
#include <QSignalMapper>

class QSizeF;

class networkstatus : public Plasma::Applet {
  Q_OBJECT
  public:
    networkstatus(QObject *parent, const QVariantList &args);
    ~networkstatus();
    void paintInterface(QPainter *painter,
                        const QStyleOptionGraphicsItem *option,
                        const QRect& contentsRect);
    void init();
    virtual QList< QAction* > contextualActions();
  private Q_SLOTS:
    void networkstatusChangedSlot(Solid::Networking::Status newstatus);
    void updateSlot();
    void overrideNetworkSettingsSlot(int newstatus);
  private:
    Solid::Networking::Status m_status;
    QTimer m_timer;
    bool m_timer_color_status;
    QList<QAction*> m_context_actions;
    QSignalMapper m_action_mapper;
};

// This is the command that links your applet to the .desktop file
K_EXPORT_PLASMA_APPLET(networkstatus, networkstatus)
#endif //NETWORKSTATUS_H