/*
    Copyright (c) 2011 Sune Vuorela <sune@vuorela.dk>

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without
    restriction, including without limitation the rights to use,
    copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following
    conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE.

*/

#include "networkstatus.h"
#include <QPainter>
#include <QSizeF>
#include <KLocale>

#include <QAction>

#include <QDBusInterface>
#include <QDBusReply>

#include <plasma/svg.h>
#include <plasma/theme.h>

networkstatus::networkstatus(QObject *parent, const QVariantList &args)
  : Plasma::Applet(parent, args),
  m_status(Solid::Networking::Unknown),
  m_timer_color_status(false) {
  connect(Solid::Networking::notifier(),SIGNAL(statusChanged(Solid::Networking::Status)),SLOT(networkstatusChangedSlot(Solid::Networking::Status)));
  connect(&m_timer,SIGNAL(timeout()),SLOT(updateSlot()));
  connect(&m_action_mapper,SIGNAL(mapped(int)),SLOT(overrideNetworkSettingsSlot(int)));

  setBackgroundHints(DefaultBackground);
  resize(200, 200);

  {
    QAction* unknown = new QAction(i18n("Set manual network to Unknown"),this);
    m_action_mapper.setMapping(unknown,static_cast<int>(Solid::Networking::Unknown));
    connect(unknown,SIGNAL(triggered()),&m_action_mapper,SLOT(map()));
    m_context_actions << unknown;

    QAction* unconnected = new QAction(i18n("Set manual network to Unconnected"),this);
    m_action_mapper.setMapping(unconnected,static_cast<int>(Solid::Networking::Unconnected));
    connect(unconnected,SIGNAL(triggered()),&m_action_mapper,SLOT(map()));
    m_context_actions << unconnected;

    QAction* disconnecting = new QAction(i18n("Set manual network to Disconnecting"),this);
    m_action_mapper.setMapping(disconnecting,static_cast<int>(Solid::Networking::Disconnecting));
    connect(disconnecting,SIGNAL(triggered()),&m_action_mapper,SLOT(map()));
    m_context_actions << disconnecting;

    QAction* connecting = new QAction(i18n("Set manual network to Connecting"),this);
    m_action_mapper.setMapping(connecting,static_cast<int>(Solid::Networking::Connecting));
    connect(connecting,SIGNAL(triggered()),&m_action_mapper,SLOT(map()));
    m_context_actions << connecting;

    QAction* connected = new QAction(i18n("Set manual network to Connected"),this);
    m_action_mapper.setMapping(connected,static_cast<int>(Solid::Networking::Connected));
    connect(connected,SIGNAL(triggered()),&m_action_mapper,SLOT(map()));
    m_context_actions << connected;
  }
}


networkstatus::~networkstatus() {
}

void networkstatus::init() {
    m_status = Solid::Networking::status();
}


void networkstatus::paintInterface(QPainter *p,
        const QStyleOptionGraphicsItem *, const QRect &contentsRect) {
  p->setRenderHint(QPainter::SmoothPixmapTransform);
  p->setRenderHint(QPainter::Antialiasing);

  int size = qMin(contentsRect.width(),contentsRect.height())/2;
  switch(m_status) {
    case 0:
      p->setBrush(Qt::gray);
      break;
    case 1:
      p->setBrush(Qt::red);
      break;
    case 2:
      if (m_timer_color_status) {
        p->setBrush(Qt::red);
      } else {
        p->setBrush(Qt::gray);
      }
      break;
    case 3:
      if (m_timer_color_status) {
        p->setBrush(Qt::green);
      } else {
        p->setBrush(Qt::gray);
      }
      break;
    case 4:
      p->setBrush(Qt::green);
      break;
    default:
      p->setBrush(Qt::black);
      break;
  }
  p->drawEllipse(contentsRect.center(),size,size);
}

void networkstatus::networkstatusChangedSlot(Solid::Networking::Status newstatus) {
  m_status = newstatus;
  if(newstatus == 2 || newstatus == 3) {
    m_timer_color_status = true;
    m_timer.start(500);
  } else {
    m_timer.stop();
  }
  update();
}

void networkstatus::updateSlot() {
  m_timer_color_status = !m_timer_color_status;
  update();
}

QList< QAction* > networkstatus::contextualActions() {
  return m_context_actions;
}

void networkstatus::overrideNetworkSettingsSlot(int newstatus) {
  Q_ASSERT(newstatus >= 0);
  Q_ASSERT(newstatus <= 4);
  QDBusInterface interface("org.kde.kded","/modules/networkstatus","org.kde.Solid.Networking.Service");
  QDBusReply<QStringList> reply = interface.call("networks");
  if(reply.isValid()) {
    if(!reply.value().contains("manual")) {
      interface.call("registerNetwork","manual",0,"manual");
    }
  }
  interface.call("setNetworkStatus","manual",newstatus);
}


#include "networkstatus.moc"
